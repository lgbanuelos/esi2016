package com.rentit.inventory.application.service;

import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryEntryID;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.inventory.rest.PlantInventoryEntryRestController;
import com.sun.corba.se.impl.interceptors.PICurrent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantInventoryEntryAssembler extends ResourceAssemblerSupport<PlantInventoryEntry, PlantInventoryEntryDTO> {

    @Autowired
    PlantInventoryEntryRepository entryRepository;

    public PlantInventoryEntryAssembler() {
        super(PlantInventoryEntryRestController.class, PlantInventoryEntryDTO.class);
    }

    @Override
    public PlantInventoryEntryDTO toResource(PlantInventoryEntry plantInventoryEntry) {
        PlantInventoryEntryDTO dto = createResourceWithId(plantInventoryEntry.getId().getId(), plantInventoryEntry);
        dto.set_id(plantInventoryEntry.getId().getId());
        dto.setName(plantInventoryEntry.getName());
        dto.setDescription(plantInventoryEntry.getDescription());
        dto.setPrice(plantInventoryEntry.getPrice());
        return dto;
    }

    public PlantInventoryEntryDTO toResource(PlantInventoryEntryID plantInfo) {
        return toResource(entryRepository.getOne(plantInfo));
    }
}
